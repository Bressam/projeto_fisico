//BLUETOOH
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
//OLED
#include "SSD1306.h"
#include "images.h" // Include custom images

#define SERVICE_UUID   "ab0828b1-198e-4351-b779-901fa0e0371e"
#define CHARACTERISTIC_UUID_RX  "4ac8a682-9736-4e5d-932b-e9b31405049c"
#define CHARACTERISTIC_UUID_TX  "0972EF8C-7613-4075-AD52-756F33D4DA91"

BLECharacteristic *characteristicTX; //através desse objeto iremos enviar dados para o client
bool deviceConnected = false; //controle de dispositivo conectado
SSD1306 display(0x3c, 5, 4);

#define DEMO_DURATION 3000
typedef void (*Demo)(void);

int demoMode = 0;
int counter = 1;
String content = "";
char character;


//callback para receber os eventos de conexão de dispositivos
class ServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
 deviceConnected = true;
    };
 
    void onDisconnect(BLEServer* pServer) {
 deviceConnected = false;
    }
};

//callback para eventos das características
class CharacteristicCallbacks: public BLECharacteristicCallbacks {
     void onWrite(BLECharacteristic *characteristic) {
          //retorna ponteiro para o registrador contendo o valor atual da caracteristica
          std::string rxValue = characteristic->getValue(); 
          //verifica se existe dados (tamanho maior que zero)
          if (rxValue.length() > 0) {
//              while(Serial.available()) {
//                  character = Serial.read();
//                  content.concat(rxValue.c_str());
//              }
           for (int i = 0; i < rxValue.length(); i++) {
             Serial.print(rxValue[i]);
             content.concat(rxValue[i]);
             Serial.println(content);
           }
          }
     }//onWrite
};
  
void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();
  //Bluetooth
  // Create the BLE Device
  BLEDevice::init("ESP32-BLE"); // nome do dispositivo bluetooth
  
  // Create the BLE Server
  BLEServer *server = BLEDevice::createServer(); //cria um BLE server 
  
  server->setCallbacks(new ServerCallbacks()); //seta o callback do server"
  
  // Create the BLE Service
  BLEService *service = server->createService(SERVICE_UUID);
  
  // Create a BLE Characteristic para envio de dados
  characteristicTX = service->createCharacteristic(
                   CHARACTERISTIC_UUID_TX,
                   BLECharacteristic::PROPERTY_NOTIFY
                 );
  
  characteristicTX->addDescriptor(new BLE2902());
    // Create a BLE Characteristic para recebimento de dados
    BLECharacteristic *characteristic = service->createCharacteristic(
                                                      CHARACTERISTIC_UUID_RX,
                                                      BLECharacteristic::PROPERTY_WRITE
                                                    );
 
    characteristic->setCallbacks(new CharacteristicCallbacks());
 
    // Start the service
    service->start();
 
    // Start advertising (descoberta do ESP32)
    server->getAdvertising()->start();
    Serial.println("Aguardando algum dispositivo conectar...");
    
  //OLED
  // Initialising the UI will init the display too.
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  //display.setTextAlignment(TEXT_ALIGN_LEFT);
}

void drawFontFaceDemo() {
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "Hello world");
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 10, "Hello world");
    display.setFont(ArialMT_Plain_24);
    display.drawString(0, 26, "Hello world");
}

void drawTextFlowDemo() {
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawStringMaxWidth(0, 0, 128,
      "Lorem ipsum\n dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore." );
}

void drawTextAlignmentDemo() {
    // Text alignment demo
  display.setFont(ArialMT_Plain_10);

  // The coordinates define the left starting point of the text
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 10, "Left aligned (0,10)");

  // The coordinates define the center of the text
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 22, "Center aligned (64,22)");

  // The coordinates define the right end of the text
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(128, 33, "Right aligned (128,33)");
}

void drawRectDemo() {
      // Draw a pixel at given position
    for (int i = 0; i < 10; i++) {
      display.setPixel(i, i);
      display.setPixel(10 - i, i);
    }
    display.drawRect(12, 12, 20, 20);

    // Fill the rectangle
    display.fillRect(14, 14, 17, 17);

    // Draw a line horizontally
    display.drawHorizontalLine(0, 40, 20);

    // Draw a line horizontally
    display.drawVerticalLine(40, 0, 20);
}

void drawCircleDemo() {
  for (int i=1; i < 8; i++) {
    display.setColor(WHITE);
    display.drawCircle(32, 32, i*3);
    if (i % 2 == 0) {
      display.setColor(BLACK);
    }
    display.fillCircle(96, 32, 32 - i* 3);
  }
}

void drawProgressBarDemo() {
  int progress = (counter / 5) % 100;
  // draw the progress bar
  display.drawProgressBar(0, 32, 120, 10, progress);

  // draw the percentage as String
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 15, String(progress) + "%");
}

void drawImageDemo() {
    // see http://blog.squix.org/2015/05/esp8266-nodemcu-how-to-create-xbm.html
    // on how to create xbm files
    display.drawXbm(34, 14, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
}

Demo demos[] = {drawFontFaceDemo, drawTextFlowDemo, drawTextAlignmentDemo, drawRectDemo, drawCircleDemo, drawProgressBarDemo, drawImageDemo};
int demoLength = (sizeof(demos) / sizeof(Demo));
long timeSinceLastModeSwitch = 0;

void loop() {
  if (deviceConnected) {
      characteristicTX->setValue("ola"); //seta o valor que a caracteristica notificará (enviar) 
      characteristicTX->notify(); // Envia o valor para o smartphone
      display.clear();
      display.drawString(0, 0, "Recebido:");
      display.drawString(0,32,content.c_str());
      display.display();
      //content = "";
  }
    delay(100);
  
//  // clear the display
//  display.clear();
//  // draw the current demo method
//  demos[demoMode]();
//
//  display.setTextAlignment(TEXT_ALIGN_RIGHT);
//  display.drawString(10, 128, String(millis()));
//  // write the buffer to the display
//  display.display();
//
//  if (millis() - timeSinceLastModeSwitch > DEMO_DURATION) {
//    demoMode = (demoMode + 1)  % demoLength;
//    timeSinceLastModeSwitch = millis();
//  }
//  counter++;
//  delay(10);
}
