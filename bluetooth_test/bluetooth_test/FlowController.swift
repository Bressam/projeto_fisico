//
//  FlowController.swift
//  bluetooth_test
//
//  Created by Giovanne Bressam on 15/08/19.
//  Copyright © 2019 Giovanne Bressam. All rights reserved.
//

import Foundation
import CoreBluetooth

class FlowController {
    
    weak var bluetoothSerivce: BluetoothService? // 1.
    
    init(bluetoothSerivce: BluetoothService) {
        self.bluetoothSerivce = bluetoothSerivce
    }
    
    func bluetoothOn() {
    }
    
    func bluetoothOff() {
    }
    
    func scanStarted() {
    }
    
    func scanStopped() {
    }
    
    func connected(peripheral: CBPeripheral) {
    }
    
    func disconnected(failure: Bool) {
    }
    
    func discoveredPeripheral() {
    }
    
    func readyToWrite() {
    }
    
    func received(response: Data) {
    }
    
    // TODO: add other events if needed
}
