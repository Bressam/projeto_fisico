//
//  BluetoothCommands.swift
//  bluetooth_test
//
//  Created by Giovanne Bressam on 15/08/19.
//  Copyright © 2019 Giovanne Bressam. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothService {
    
    func getSettings() {
        self.peripheral?.readValue(for: self.dataCharacteristic!)
    }
    
    // TODO: add other methods to expose high level requests to peripheral
}
